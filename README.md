##  Learning Three.js - the JavaScript 3D Library for WebGL
This repository contains my take on the second edition of the e-book [Learning Three.js - the JavaScript 3D Library for WebGL](https://www.packtpub.com/web-development/learning-threejs-javascript-3d-library-webgl-second-edition).

### Requirements
- [Node.js](https://nodejs.org/en/) (Tested version 12.13.0)
- [Python](https://www.python.org/) (Tested version 3.7.4)
- a browser with [WebGL capability](https://caniuse.com/#feat=webgl)

### Install requirements
```
npm i
```

### Get started
Then call the server via

```
npm start
```

### Alternative with Python:
If you do not want to install via NPM and you do have installed Python 3.6+ installed type the following command from within this repository:

```
python3 -m http.server 9000
```

This creates a local server on port 9000, which you can open within your browser at `localhost:9000`.

NB: Depending on your installation, replace `python3` by `python`

## Table Of Content
### Chapter 1
![First Scene](./assets/gifs/chapter_01.02.gif)
![Material and Light](./assets/gifs/chapter_01.03.gif)
![Material, Light and Animation](./assets/gifs/chapter_01.04.gif)
![Control GUI](./assets/gifs/chapter_01.05.gif)
![Screen size change](./assets/gifs/chapter_01.06.gif)

### Chapter 2
![Basic Scene](./assets/gifs/chapter_02.01.gif)
